import {Row, Col, Card, Button} from 'react-bootstrap';

export default function CourseCard() {
  return (
    <Card>
      <Card.Body>
        <Card.Title>Sample Course</Card.Title>
        <Card.Text>
        	<Col>Description:</Col>	
         	<Col className="pb-2">This is a sample course offering.</Col>	
         	<Col>Price</Col>	
         	<Col>PhP 40,000</Col>		      	
        </Card.Text>
        <Button variant="primary">Enroll</Button>
      </Card.Body>
    </Card>
  );
}